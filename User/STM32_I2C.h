#ifndef __STM32_I2C_H
#define __STM32_I2C_H
#include "stm32f30x.h"
#include <stm32f30x_i2c.h>
#include <stm32f30x_gpio.h>
#include <stm32f30x_rcc.h>
#define I2C_TIMEOUT 400
#define DeviceAddr 0x69
/*====================================================================================================*/
/*====================================================================================================*/
//int8_t i2cWrite(uint8_t addr_, uint8_t reg_, uint8_t data);
void i2cInit(void);

int8_t i2c_write(uint8_t addr, uint8_t reg, uint8_t len, uint8_t * data);
int8_t i2c_read(uint8_t addr, uint8_t reg, uint8_t len, uint8_t *buf);
/*====================================================================================================*/
/*====================================================================================================*/
#endif
