#include "MPU6050.h"

void MPU6050_init()
{     
	i2cWrite(0x23,0x6B,0x80);//reset
	delay_ms(50);
	i2cWrite(0x23,0x6B,0x01);// 退出睡眠模式，设取样时钟为陀螺X轴。
	delay_ms(100);
	i2cWrite(0x23,0x19,3);//Fsample=1K/(3+1)=250Hz
	delay_ms(10);
	i2cWrite(0x23,0x1A,2);//LPF Fsample=1K BW=184Hz for A,delay=2ms; 188Hz for G, dealy=1.9ms;
	delay_ms(10);
	i2cWrite(0x23,0x1B,3<<3);//Gryo scale=+-2000 deg/s
	delay_ms(10);
	i2cWrite(0x23,0x1C,16);//Acc scale=+-8G
	delay_ms(100);
	//run_self_test();

}

// {寄存器地址,寄存器值},
//{0x6B,1     }, // 退出睡眠模式，设取样时钟为陀螺X轴。
//{0x19,4     }, // 取样时钟4分频，1k/4，取样率为250Hz。
//{0x1A,2     }, // 低通滤波，截止频率100Hz左右。
//{0x1B,3<<3  }, // 陀螺量程，2000dps。
//{0x1C,2<<3  }, // 加速度计量程，8g。

__inline void run_self_test()
{
	i2cWrite(0x23,0x1B,3<<3);//Gryo scale=+-2000 deg/s
	delay_ms(10);
	i2cWrite(0x23,0x1C,16);//Acc scale=+-8G
	delay_ms(100);
}
