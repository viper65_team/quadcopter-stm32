#include "main.h"

int16_t value[10];//result -> value
uint8_t result[14];//from mpu6050 ,raw data
uint16_t Speed=22;
uint8_t flag_power=1;
__IO uint16_t Battery=0;
__IO uint8_t test_flag=0,flag_flying=0,flag_fly=0,flag_jeopardy=0;
int main(void)
{
	flag_calculate = 0;
	flag_coms = 0;
	flag_comr = 0;
	flag_timeout=0;
	
	gpio_init();
	DMA_init();
	tim3_init();
	tim4_init();
	tim2_init();
	uart_init();
	i2cInit();
	MPU6050_init();
	adc_init();	
	nvic_init();
	
  while(1)
  {
		if(flag_calculate)
		{
			Calculate();
			Control();
			flag_calculate=0;
			test_flag=0;
		}
		
		if(flag_coms)
		{
			if(flag_coms==2)
			{
				if(!flag_jeopardy)
					flag_fly=1;
				UpdateTarget();
			}
			else
				flag_fly=0;
			
			if(flag_power)
				Updata2COM(0x38);
			else
				Updata2COM(0x83);
			USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
			commucating_flag=0;
			flag_coms=0;
		}

		if(flag_flying!=flag_fly)
		{
			if(flag_fly&&flag_power)
			{
					noComm_flag=0;
					vx_acc=0;
					vy_acc=0;
					x_z1=0;
					x_z2=0;
					x_z3=0;
					y_z1=0;
					y_z2=0;
					y_z3=0;
					TIM2_PWMENABLE();
			}
			else
			{
					TIM2_PWMDISABLE();
					noComm_flag=0;
			}
			flag_flying=flag_fly;
		}
		
		if(flag_comr)
		{
			switch(flag_comr)
			{
				case 1:
					if(!flag_flying)
						UpdateXYPID();
					USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
				break;
				case 2:
					if(!flag_flying)
						UpdateZPID();
					USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
				break;
			}
			flag_comr=0;
		}

		if(flag_flying&&(noComm_flag||flag_jeopardy))
		{
			TIM2_PWMDISABLE();
			flag_fly=0;
		}
  }
}

void gpio_init()
{
  GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOE,ENABLE );
	RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA,ENABLE );	

  GPIO_InitStructure.GPIO_Mode=GPIO_Mode_OUT;				  
  GPIO_InitStructure.GPIO_Speed=GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_UP ;
  
  GPIO_InitStructure.GPIO_Pin= GPIO_Pin_8;
  GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	GPIO_SetBits(GPIOA,GPIO_Pin_8);
///LED
	
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_NOPULL;
  
  GPIO_InitStructure.GPIO_Pin= GPIO_Pin_1;
  GPIO_Init(GPIOA,&GPIO_InitStructure);
//BAT_AD ADC1_IN2
}

void adc_init()
{
  ADC_InitTypeDef    ADC_InitStructure;
  ADC_CommonInitTypeDef ADC_CommonInitStructure;
	
	RCC_ADCCLKConfig(RCC_ADC12PLLCLK_Div64);
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC12,ENABLE);
	
	ADC_StructInit(&ADC_InitStructure);
	
	ADC_VoltageRegulatorCmd(ADC1,ENABLE);
	
	delay_ms(10);
	
	ADC_SelectCalibrationMode(ADC1,ADC_CalibrationMode_Single);
	ADC_StartCalibration(ADC1);
	
	while(ADC_GetCalibrationStatus(ADC1)!=RESET);
	//calibration_value
	
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Clock = ADC_Clock_AsynClkMode;  
  ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
  ADC_CommonInitStructure.ADC_DMAMode = ADC_DMAMode_OneShot;
  ADC_CommonInitStructure.ADC_TwoSamplingDelay = 0;
	
	ADC_CommonInit(ADC1,&ADC_CommonInitStructure);
	
	ADC_InitStructure.ADC_ContinuousConvMode=ADC_ContinuousConvMode_Enable;
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_8b;
  ADC_InitStructure.ADC_ExternalTrigConvEvent = ADC_ExternalTrigConvEvent_0;
  ADC_InitStructure.ADC_ExternalTrigEventEdge = ADC_ExternalTrigEventEdge_None;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_OverrunMode = ADC_OverrunMode_Disable;
  ADC_InitStructure.ADC_AutoInjMode = ADC_AutoInjec_Disable;
  ADC_InitStructure.ADC_NbrOfRegChannel = 1;
  ADC_Init(ADC1, &ADC_InitStructure);
	
	ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 1, ADC_SampleTime_2Cycles5);
	ADC_AnalogWatchdog2SingleChannelConfig(ADC1, ADC_Channel_2);
	ADC_AnalogWatchdog2ThresholdsConfig(ADC1, 0xFF, 0x80);//1.65V*2=3.3V

	ADC_ITConfig(ADC1, ADC_IT_AWD2, ENABLE);
	
	ADC_Cmd(ADC1, ENABLE);

	while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_RDY));
	
	ADC_DMACmd(ADC1,ENABLE);
	ADC_DMAConfig(ADC1,ADC_DMAMode_Circular);
	
	ADC_StartConversion(ADC1);

}

void nvic_init()
{
	NVIC_InitTypeDef NVIC_initStructure;
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	NVIC_initStructure.NVIC_IRQChannel=TIM3_IRQn;
	NVIC_initStructure.NVIC_IRQChannelPreemptionPriority=2;//high
	NVIC_initStructure.NVIC_IRQChannelSubPriority=2;
	NVIC_initStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_initStructure);	
	
	NVIC_initStructure.NVIC_IRQChannel=TIM4_IRQn;
	NVIC_initStructure.NVIC_IRQChannelPreemptionPriority=0;//high
	NVIC_initStructure.NVIC_IRQChannelSubPriority=0;
	NVIC_initStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_initStructure);	
	
	NVIC_initStructure.NVIC_IRQChannel = DMA1_Channel3_IRQn;
	NVIC_initStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_initStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_initStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_initStructure);

	NVIC_initStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_initStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_initStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_initStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_initStructure);

  NVIC_initStructure.NVIC_IRQChannel = ADC1_2_IRQn;      
  NVIC_initStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_initStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_initStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_initStructure);

	SysTick->CTRL  = 0x06;
}

void delay_ms(u32 n)
{
		SysTick->LOAD=64000;//1ms for 64MHz
		SysTick->VAL=0;
		TimingDelay=n;
		SysTick->CTRL=0x07;
		while(TimingDelay);
}

void delay_us(u32 n)
{
		SysTick->LOAD=64;//1us for 64MHz
		SysTick->VAL=0;
		TimingDelay=n;
		SysTick->CTRL=0x07;
		while(TimingDelay);
}

void ADC1_2_IRQHandler(void)
{
	if(ADC_GetFlagStatus(ADC1, ADC_FLAG_AWD2) != RESET)
  {
		//no power
		ADC_ITConfig(ADC1, ADC_IT_AWD2, DISABLE);
    ADC_ClearITPendingBit(ADC1, ADC_IT_AWD2);
		flag_power=0;
  }
}
