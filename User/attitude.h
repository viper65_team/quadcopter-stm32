#ifndef __ATTITUDE
#define __ATTITUDE

#include "main.h"
#include "quaternion.h"
#include "MahonyAHRS.h"

void Calculate(void);
void UpdateTarget(void);

extern __IO float yaw;//used by pid
extern __IO float q0, q1, q2, q3;
extern __IO quaternion attitude_c;
extern __IO char flag_bias;
#endif
