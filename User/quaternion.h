#ifndef __QUAT_H
#define __QUAT_H

typedef struct
{
	float w,x,y,z;
}quaternion;

float getLength(quaternion q);
quaternion normalize(quaternion q);
quaternion quatmulti(quaternion q1,quaternion q2);
quaternion quatinv(quaternion p);
//quaternion quatrotate(quaternion p,quaternion rotate);//rotate must be a unit quaternion

__inline quaternion conjugate(quaternion q)
{
	q.x=-q.x;
	q.y=-q.y;
	q.z=-q.z;
	return q;
}

#endif
