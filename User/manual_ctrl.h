#ifndef __MANUAL_CTRL
#define __MANUAL_CTRL

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "quaternion.h"
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported variable  ------------------------------------------------------- */
extern __IO quaternion t;
extern __IO float bias_yaw;
extern __IO char flag_tChanged;
/* functions ---------------------------------------------------------------- */
void UpdateTarget(void);

#endif /* MANUAL_CTRL */
