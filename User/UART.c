#include "UART.h"

__IO char flag_coms,flag_comr,flag_timeout=0;
__IO char info_s[27],info_g[12];

void PrintChar(char* ch)
{
	while(*ch)
	{
		PutChar(*ch++);
	}
}

void PrintArray(char *ch,char length)
{
	while(length--)
	{
		PutChar(*ch++);
	}
}

void uart_init()
{
	USART_InitTypeDef USART_initStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);
	
	pin_init();
	
	USART_initStructure.USART_BaudRate = 115200;
	USART_initStructure.USART_WordLength = USART_WordLength_8b;
	USART_initStructure.USART_StopBits = USART_StopBits_1;
	USART_initStructure.USART_Parity = USART_Parity_No;
	USART_initStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_initStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART3, &USART_initStructure);
  
	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
	USART_ITConfig(USART3, USART_IT_TXE, DISABLE);
	USART_OverrunDetectionConfig(USART3,USART_OVRDetection_Disable);
	/* Enable USART */
	USART_Cmd(USART3, ENABLE);

	USART_DMACmd(USART3, USART_DMAReq_Tx, ENABLE);
	USART_DMACmd(USART3, USART_DMAReq_Rx, ENABLE);
}

void pin_init()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB,ENABLE);
	
	GPIO_PinAFConfig(GPIOB,GPIO_PinSource10,GPIO_AF_7);
	GPIO_PinAFConfig(GPIOB,GPIO_PinSource11,GPIO_AF_7);
	
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF;											  
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType=GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_UP ;
  
	GPIO_InitStructure.GPIO_Pin= GPIO_Pin_10|GPIO_Pin_11;
	GPIO_Init(GPIOB,&GPIO_InitStructure);
}

void float2string(float value, char* str)//3bit dec+ point +3bit dec
{
	uint8_t integer,fraction;
	if(value<0)
	{
		str[0]='-';
		value=0-value;
	}
	else
	{
		str[0]='+';
	}
	integer=(uint8_t)value;
	fraction=(uint8_t)((value-integer)*1000);
	str[8]=0;
	str[4]='.';
	int2string(integer,str);
	int2string(fraction,&str[5]);
}

void int2string(int value, char* str)//3bit dec
{
	int count;
	count%=1000;
	for(count=2;count>=0;count--)
	{
		str[count]=value%10+48;
		value/=10;
	}
}

void Updata2COM(int no)
{
	int16_t count=0;

	info_s[0]=no;
	
	*((int16_t*)&info_s[1])=value[0];
	*((int16_t*)&info_s[3])=value[1];
	*((int16_t*)&info_s[5])=value[2];
	*((int16_t*)&info_s[7])=value[3];
	count=value[0]+value[1]+value[2]+value[3];
	*((int16_t*)&info_s[9])=count;
	
	*((int16_t*)&info_s[11])=value[4];
	*((int16_t*)&info_s[13])=value[5];
	*((int16_t*)&info_s[15])=value[6];
	count=value[4]+value[5]+value[6];
	*((int16_t*)&info_s[17])=count;
	
	*((int16_t*)&info_s[19])=value[7];
	*((int16_t*)&info_s[21])=value[8];
	*((int16_t*)&info_s[23])=value[9];	
	count=value[7]+value[8]+value[9];
	*((int16_t*)&info_s[25])=count;
	
 	DMA_Cmd(DMA1_Channel2, DISABLE);
 	DMA_SetCurrDataCounter(DMA1_Channel2, 27);
 	DMA_Cmd(DMA1_Channel2, ENABLE);

}

void USART3_IRQHandler(void)
{
	char command;
	if(USART_GetITStatus(USART3,USART_IT_TXE))
	{
		USART_ClearFlag(USART3,USART_IT_TXE);
	}
	if (USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
	{
		command = (char)USART_ReceiveData(USART3);//RXNE is cleared by a read to the USART_RDR register.

		if (command == 0x37)//直接返回或读取后返回
		{
			flag_coms = 1;
			flag_fly=0;
			return;
		}
		if(command==0x38)
		{
			USART_ITConfig(USART3, USART_IT_RXNE, DISABLE);
			flag_timeout=1;
			DMA_Cmd(DMA1_Channel3, DISABLE);
			DMA_SetCurrDataCounter(DMA1_Channel3, 8);
			DMA_Cmd(DMA1_Channel3, ENABLE);
			return;
		}
		if (command == 0x39)//xy pid
		{
			USART_ITConfig(USART3, USART_IT_RXNE, DISABLE);
			flag_timeout=1;
			DMA_Cmd(DMA1_Channel3, DISABLE);
			DMA_SetCurrDataCounter(DMA1_Channel3, 8);
			DMA_Cmd(DMA1_Channel3, ENABLE);
			return;
		}
		if (command == 0x3a)//z pid
		{
			USART_ITConfig(USART3, USART_IT_RXNE, DISABLE);
			flag_timeout=1;
			DMA_Cmd(DMA1_Channel3, DISABLE);
			DMA_SetCurrDataCounter(DMA1_Channel3, 8);
			DMA_Cmd(DMA1_Channel3, ENABLE);
			return;
		}
		if(command==0x3b)
		{
			flag_bias=2;
			return;
		}
	}
}

