#ifndef __MPU6050
#define __MPU6050

/* Includes ------------------------------------------------------------------*/
#include "stm32f30x.h"
#include "main.h"

#include "STM32_I2C.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/* functions ---------------------------------------------------------------- */
extern void i2cInit(void);

void MPU6050_init(void);

#endif /* __MPU6050 */
