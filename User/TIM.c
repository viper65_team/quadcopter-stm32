#include "TIM.h"

__IO uint8_t flag_calculate,commucating_flag=0,noComm_flag=0;

void tim3_init()
{	
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	
	RCC_APB1PeriphClockCmd(	RCC_APB1Periph_TIM3, ENABLE);
	
	TIM_TimeBaseStructure.TIM_Prescaler=3199;//20k Prescaler = (TIM3CLK / TIM3 counter clock) - 1
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1 ;//20k
	TIM_TimeBaseStructure.TIM_Period=79;//19+1=20->4ms 250Hz
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
	
	TIM_Cmd(TIM3, ENABLE);

}

void tim2_init(void)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA|RCC_AHBPeriph_GPIOB,ENABLE);
	//PA0 PA3 PA9 PB3
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_0|GPIO_Pin_3|GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_OType=GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_UP;
	

	GPIO_Init(GPIOA,&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_3;
	
	GPIO_Init(GPIOB,&GPIO_InitStructure);
	
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource0,GPIO_AF_1);//ch1
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource3,GPIO_AF_1);//ch4
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource9,GPIO_AF_10);//ch3
	GPIO_PinAFConfig(GPIOB,GPIO_PinSource3,GPIO_AF_1);//ch2
	
	RCC_APB1PeriphClockCmd(	RCC_APB1Periph_TIM2, ENABLE);
	
	TIM_TimeBaseStructure.TIM_Prescaler=0;//64M Prescaler = (TIM2CLK / TIM2 counter clock) - 1
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1 ;//1M
	TIM_TimeBaseStructure.TIM_Period=6399;//6399->f=10K
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
	
	TIM_OCInitStructure.TIM_OCMode=TIM_OCMode_Asymmetric_PWM1;
	TIM_OCInitStructure.TIM_OCPolarity=TIM_OCPolarity_High;//output=H Gate->Open
	TIM_OCInitStructure.TIM_Pulse=1280;//两块电池需要近3000
	
	TIM_OC1Init(TIM2,&TIM_OCInitStructure);
	TIM_OC2Init(TIM2,&TIM_OCInitStructure);
	TIM_OC3Init(TIM2,&TIM_OCInitStructure);
	TIM_OC4Init(TIM2,&TIM_OCInitStructure);
	
	TIM_Cmd(TIM2,ENABLE);
	
	//TIM_CtrlPWMOutputs(TIM2,ENABLE);
	
}

void tim4_init()
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	
	RCC_APB1PeriphClockCmd(	RCC_APB1Periph_TIM4, ENABLE); 
	
	TIM_TimeBaseStructure.TIM_Prescaler=3199;//20KHz Prescaler = (TIM3CLK / TIM3 counter clock) - 1
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1 ;//20k
	TIM_TimeBaseStructure.TIM_Period=19;//19+1=20=> 1KHz 1ms
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);

	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
	
	TIM_Cmd(TIM4, ENABLE);
}

void TIM3_IRQHandler(void)
{
	if(TIM_GetFlagStatus(TIM3,TIM_FLAG_Update))
	{
		flag_calculate=1;
		
		if(test_flag)
			test_flag++;
		else
			test_flag=1;
		
		i2c_read(0x23, 0x3B, 14, result);
		TIM_ClearFlag(TIM3,TIM_FLAG_Update);
	}
}

void TIM4_IRQHandler(void)
{
	if(TIM_GetFlagStatus(TIM4,TIM_FLAG_Update))
	{
		if(flag_flying)
		{
			commucating_flag++;
			if(commucating_flag>200)//20ms
			{
				noComm_flag=1;
			}
		}
		if(flag_timeout)
		{
			flag_timeout++;
			if(flag_timeout>10)
			{
				USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
				flag_timeout=0;
			}
		}
		TIM_ClearFlag(TIM4,TIM_FLAG_Update);
	}
}
