#include "quaternion.h"

__inline float invSqrt(float x);

float getLength(quaternion q)
{
	float sum=q.w*q.w+q.x*q.x+q.y*q.y+q.z*q.z;
	sum=invSqrt(sum);
	sum=1/sum;
	return sum;
}

quaternion normalize(quaternion q)
{
	float NormInv=q.w*q.w+q.x*q.x+q.y*q.y+q.z*q.z;
	NormInv=invSqrt(NormInv);
	q.w*=NormInv;
	q.x*=NormInv;
	q.y*=NormInv;
	q.z*=NormInv;
	return q;
}

quaternion quatmulti(quaternion p,quaternion q)
{
	quaternion res;

	res.w=q.w*p.w-q.x*p.x-q.y*p.y-q.z*p.z;
	res.x=q.x*p.w+q.w*p.x-q.y*p.z+q.z*p.y;
	res.y=q.y*p.w+q.w*p.y-q.z*p.x+q.x*p.z;
	res.z=q.z*p.w+q.w*p.z-q.x*p.y+q.y*p.x;
	return res;
}

quaternion quatinv(quaternion p)
{
	quaternion pc,res;
	float ppp;
	pc.w=p.w;
	pc.x=-p.x;
	pc.y=-p.y;
	pc.z=-p.z;
	ppp=p.w*p.w+p.x*p.x+p.y*p.y+p.z*p.z;
	res.w=pc.w/ppp;
	res.x=pc.x/ppp;
	res.y=pc.y/ppp;
	res.z=pc.z/ppp;
	return res;
}

__inline float invSqrt(float x) {
	float halfx = 0.5f * x;
	float y = x;
	long i = *(long*)&y;
	i = 0x5f3759df - (i>>1);
	y = *(float*)&i;
	y = y * (1.5f - (halfx * y * y));
	return y;
}
