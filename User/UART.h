#ifndef __UART_H
#define __UART_H

#include <stm32f30x_usart.h>
#include <stm32f30x_gpio.h>
#include "main.h"

void uart_init(void);
void pin_init(void);
void PrintChar(char* ch);
void PrintArray(char *ch,char length);
void float2string(float value,char* str);
void int2string(int value,char* str);
void Updata2COM(int no);
void Updata2Speed(void);

extern __IO uint16_t Yaw,Roll,Pitch;
extern __IO char flag_coms, flag_comr,flag_timeout;
extern __IO char info_s[27], info_g[12];
extern __IO float kp,kd;

__inline void PutChar(uint8_t ch)
{
	USART_SendData(USART3,ch);
	
	while(!USART_GetFlagStatus(USART3, USART_FLAG_TC))
	{}

}

#endif
