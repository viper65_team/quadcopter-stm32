#ifndef __MAIN_H
#define __MAIN_H

#define DeviceAddr 0x69

#define SampleT 0.004f
#define SampleF 250.0f

/* Includes ------------------------------------------------------------------*/
#include "stm32f30x.h"
#include "MPU6050.h"
#include "STM32_I2C.h"
#include "UART.h"
#include "TIM.h"
#include "DMA.h"
#include "attitude.h"
#include "control.h"
#include "manual_ctrl.h"
#include "arm_math.h"
#include <stm32f30x_gpio.h>
#include <stm32f30x_rcc.h>
#include <stm32f30x_misc.h>
#include <stm32f30x_exti.h>
#include <stm32f30x_syscfg.h>

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void delay_us(u32 n);
void delay_ms(u32 n);

/* functions ---------------------------------------------------------------- */
void gpio_init(void);
void adc_init(void);
void uart_init(void);
void nvic_init(void);
/* variable ---------------------------------------------------------------- */
extern int16_t value[10];
extern uint8_t result[14];
extern uint16_t Speed;
extern __IO uint16_t Battery;
/* extern ---------------------------------------------------------------- */
extern __IO uint32_t TimingDelay;
extern __IO char flag_coms,flag_comr;
extern __IO uint8_t flag_flying,flag_fly,flag_jeopardy;
extern __IO float vx_acc,vy_acc;
extern __IO uint8_t test_flag,commucating_flag,noComm_flag,flag_calculate;
#endif /* __MAIN_H */
