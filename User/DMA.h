#ifndef __DMA_H
#define __DMA_H

#include "main.h"
#include <stm32f30x_dma.h>

#define USART3_DR_Base 0x40004804

void DMA_init(void);
void DMA1_Channel3_IRQHandler(void);

extern __IO char info_s[27],info_g[12];
#endif
