#include "control.h"

__IO float kp_z=800,kd_z=10;
__IO float vx_acc=0,vy_acc=0;
float speed1=0,speed2=0,speed3=0,speed4=0;
float speed_real=0;
char flag_transient=2;

#define abs(x) ((x)<0 ? -(x) : (x))
#define sign(x) ((x)<0 ? -1 : 1)

//for TD
__IO float delta=300.0f;
//for eso
__IO float beta1=100,beta2=600,beta3=12000;//100*100/3,100,32
__IO float alfa1=0.5f,alfa2=0.25f,delta_eso=0.025,b_x=0.03,b_y=0.03;
__IO float x_z1=0,x_z2=0,x_z3=0,y_z1=0,y_z2=0,y_z3=0;
//end for NLSEF
__IO float c=10.0f,r=900.0f,h1=0.030f;//

__inline void GetTYaw(void);
__inline void cCheck(void);
__inline float invSqrt(float x);
void TDX(float *r1,float *r2,float origin,float delta);
void TDY(float *r1,float *r2,float origin,float delta);
void ESOX(float y,float h,float u);
void ESOY(float y,float h,float u);
float fhan(float e1,float e2,float c,float r,float h1);
char transient(char Ischanged,quaternion t, quaternion *ct ,quaternion *ctd);

void Control()//速度给定范围在100以内，不包括100
{
	quaternion e;
	float speed1=0,speed2=0,speed3=0,speed4=0;
	//static float vx_old,vy_old,vx_dif=0,vy_dif=0;
	static float z_eold=0,z_dif,px=0,py=0,pz,z_e;
	static quaternion ct={1,0,0,0},ct_diff;
	float vx,vy,vx_TDdif=0,vx_TD=0,vy_TDdif=0,vy_TD=0;
	
	
	GetTYaw();//calculates the target-yaw
	cCheck();//safty check
	
//	if((!flag_transient)||flag_tChanged)
//	{
//		flag_tChanged=0;
//		flag_transient=transient(flag_tChanged,t,&ct,&ct_diff);

//		//t-c=>e,the orientaion err.
//		//e=quatmulti(conjugate(attitude_c),ct);
//			
//	}
//	
//	if(flag_transient==1)
//	{
//		ct.x=t.x;
//		ct.y=t.y;
//		ct.w=t.w;
//		
//		//t-c=>e,the orientaion err.
//		//e=quatmulti(conjugate(attitude_c),ct);
//		
//		ct_diff.x=0;
//		ct_diff.y=0;
//		
//		flag_transient=2;
//	}
	
		//t-c=>e,the orientaion err.
		//e=quatmulti(conjugate(attitude_c),ct);
	e=quatmulti(conjugate(attitude_c),t);

	//unit quaternion's invertion = its conjugate
	if(q0>0)//when theta is greater than 2pi, theta/2>pi,the value of sin(theta/2) will be inversed.
	{
		vx=e.x;
		vy=e.y;
	}
	else
	{
		vx=-e.x;
		vy=-e.y;
	}
	
	TDX(&vx_TD,&vx_TDdif,vx,delta);
	TDY(&vy_TD,&vy_TDdif,vy,delta);

	//eso use the last u(with compensation),not the current u(without compensation).
	ESOX(-vx,0.004,px);
	ESOY(-vy,0.004,py);

	{//limits the acculation value
	vx_acc+=vx;
	if(vx_acc>1.0f)
		vx_acc=1.0f;
	if(vx_acc<-1.0f)
		vx_acc=-1.0f;
	
	vy_acc+=vy;
	if(vy_acc>1.0f)
		vy_acc=1.0f;
	if(vy_acc<-1.0f)
		vy_acc=-1.0f;
	}
	
//calculate the Torque
	px=fhan(-vx,-vx_TDdif,c,r,h1)+vx_acc*20;//2000*5*0.004=40
	py=fhan(-vy,-vy_TDdif,c,r,h1)+vy_acc*20;
	
//compensate the disturbance.
//px=px-x_z3/b_x;
//py=py-y_z3/b_y;
	
{//for yaw
	z_e=0-yaw;
	z_dif=z_e-z_eold;
	z_eold=z_e;
	pz=kp_z*(z_e+z_dif*kd_z);
}
	
//combine the output	
	speed1=-px-py+pz;
	speed2=-px+py-pz;
	speed3=px+py+pz;
	speed4=px-py-pz;
	

{//change the torque to motor speed , cuz F=w^2*C,in which C is a constant
	if(speed1<0)
	{
		arm_sqrt_f32(-speed1,&speed1);
		speed1=-speed1;
	}
	else
		arm_sqrt_f32(speed1,&speed1);
	
	if(speed2<0)
	{
		arm_sqrt_f32(-speed2,&speed2);
		speed2=-speed2;
	}
	else
		arm_sqrt_f32(speed2,&speed2);
	
	if(speed3<0)
	{
		arm_sqrt_f32(-speed3,&speed3);
		speed3=-speed3;
	}
	else
		arm_sqrt_f32(speed3,&speed3);
	
	if(speed4<0)
	{
		arm_sqrt_f32(-speed4,&speed4);
		speed4=-speed4;
	}
	else
		arm_sqrt_f32(speed4,&speed4);
}
	

//compensate the output which is infected by voltage drop.
	speed_real=Speed*(140.0f/Battery);

//add basic speed
	speed1+=speed_real;
	speed2+=speed_real;
	speed3+=speed_real;
	speed4+=speed_real;
	
{//limit the output	
	if(speed1<3)
		speed1=3;
	if(speed1>97)
		speed1=97;
	
	if(speed2<3)
		speed2=3;
	if(speed2>97)
		speed2=97;
	
	if(speed3<3)
		speed3=3;
	if(speed3>97)
		speed3=97;
	
	if(speed4<3)
		speed4=3;
	if(speed4>97)
		speed4=97;
}
	

//change the speed of the motors
	TIM2_Ch1(speed1);
	TIM2_Ch2(speed2);
	TIM2_Ch3(speed3);
	TIM2_Ch4(speed4);

//perare the date for graph
//value[4]=x_z1*6000;
	value[4]=x_z3/b_x*10;
//value[5]=-vx_TDdif*3000;
	value[6]=x_z2*3000;	
//value[4]=px*10;
	value[5]=px*10;
//value[6]=vx_acc*ki*kp*10;

//value[7]=y_z1*6000;//py*10;
	value[7]=y_z3/b_y*10;
	value[8]=-vy_TDdif*3000;
	value[9]=y_z2*3000;
	
}

char transient(char Ischanged,quaternion t, quaternion *ct ,quaternion *ctd)
{
	static int time=0;
	static float v3,v2,dif_x,dif_y;
	
	//f=250hz,t=0.004,T=0.5,cycle=50
	
	if(Ischanged)
	{
		v2=0;
		time=0;
		dif_x=t.x-ct->x;
		dif_y=t.y-ct->y;
	}
	else
	{
		time++;
	}
	if(time<49)
	{
		if(time<25)
		{
			v3=0.0016;
		}
		else
		{
			v3=-0.0016;
		}
		
		v2+=v3;
		ct->x+=v2*dif_x;
		ct->y+=v2*dif_y;
		arm_sqrt_f32((1-ct->x*ct->x-ct->y+ct->y),&(ct->w));
		return 0;
	}

	return 1;

	
}

float fst(float x1,float x2,float delta)
{
    float d=delta*SampleT;
    float y=x1+SampleT*x2;
    float a0,a;
		
		arm_sqrt_f32(d*d+8*delta*abs(y),&a0);
    if(abs(y)>SampleT*d)
        a=x2+(a0-d)/2*sign(y);
    else
        a=x2+y/SampleT;

    if(abs(a)>d)
        return (-delta*sign(a));
    else
        return (-delta*a/d);
}

float fal(float e,float alfa,float delta)
{
	if((e<delta)&&(e>-delta))
		return e/pow(delta,1-alfa);
	else
    return pow(abs(e),alfa)*sign(e);
}

float fhan(float e1,float e2,float c,float r,float h1)
{
	float d,ce2,a,a0,a1,a2,y,sy,sa;
	d=r*h1*h1;
	ce2=c*e2;
	a0=h1*ce2;
	y=e1+a0;
	a1=sqrt(d*(d+8*abs(y)));
	a2=a0+sign(y)*(a1-d)/2;
	sy=(sign(y+d)-sign(y-d))/2;
	a=(a0+y-a2)*sy+a2;
	sa=(sign(a+d)-sign(a-d))/2;
	return -r*(a/2-sign(a))*sa-r*sign(a);
}

void TDX(float *r1,float *r2,float origin,float delta)
{
    static float r1_old=0,r2_old=0;

    *r1=r1_old+SampleT*r2_old;
    *r2=r2_old+SampleT*fst(r1_old-origin,r2_old,delta);

    r1_old=*r1;
    r2_old=*r2;
}

void TDY(float *r1,float *r2,float origin,float delta)
{
    static float r1_old=0,r2_old=0;

    *r1=r1_old+SampleT*r2_old;
    *r2=r2_old+SampleT*fst(r1_old-origin,r2_old,delta);

    r1_old=*r1;
    r2_old=*r2;
}

//求得z1，z2后用这两个值与过渡过程的y1，y2做差，得到e1，e2。使用e1，e2来调整输出
void ESOX(float y,float h,float u)
{
	float e=x_z1-y;

	x_z1+=h*(x_z2-beta1*e);
	x_z2+=h*(x_z3-beta2*fal(e,alfa1,delta_eso)+b_x*u);
	x_z3+=-h*beta3*fal(e,alfa2,delta_eso);
}
//x_z1,x_z2,x_z3,y_z1,y_z2,y_z3
void ESOY(float y,float h,float u)
{
	float e=y_z1-y;

	y_z1+=h*(y_z2-beta1*e);
	y_z2+=h*(y_z3-beta2*fal(e,alfa1,delta_eso)+b_y*u);
	y_z3+=-h*beta3*fal(e,alfa2,delta_eso);
}

__inline void GetTYaw(void)
{
	static char phalfpi_flag=0,nhalfpi_flag=0;
	
	//expand the yaw domain
	yaw+=bias_yaw;
	
	if(yaw>1.57f&&!nhalfpi_flag)
		phalfpi_flag=1;
	if(yaw<1.57f&&yaw>0)
	{
		phalfpi_flag=0;
	}
	if(phalfpi_flag&&yaw<0)
		yaw=6.28f+yaw;
	
	if(yaw<-1.57&&!phalfpi_flag)
		nhalfpi_flag=1;
	if(yaw>-1.57f&&yaw<0)
	{
		nhalfpi_flag=0;
	}
	if(nhalfpi_flag&&yaw>0)
		yaw=yaw-6.28f;
}

__inline void cCheck(void)
{
	float sin;
	
	arm_sqrt_f32(1.0f-attitude_c.w*attitude_c.w,&sin);
	if(sin>0.45f)
	{
		flag_jeopardy=1;//protection
	}
}

void UpdateXYPID()//5,2000,0.006
{
	char sendback,sum=0,i;
	for(i=1;i<7;i++)
	{
		sum+=info_g[i];
	}
	if(sum==info_g[7])
	{
//		kp=*(uint16_t*)&info_g[1]*10;
//		ki=*(uint16_t*)&info_g[3]/10.0f;
//		kd=*(uint16_t*)&info_g[5]/100.0f;
		h1=*(uint16_t*)&info_g[1]/1000.0f;
		r=*(uint16_t*)&info_g[3]*100;
		c=*(uint16_t*)&info_g[5]/10.0f;
		
		sendback=info_g[0];
		PrintChar(&sendback);
	}
}

void UpdateZPID()
{
	char sendback,sum=0,i;
	for(i=1;i<7;i++)
	{
		sum+=info_g[i];
	}
	if(sum==info_g[7])
	{
		b_x=*(uint16_t*)&info_g[1]/1000.0f;
		b_y=*(uint16_t*)&info_g[3]/1000.0f;
		delta_eso=*(uint16_t*)&info_g[5]/1000.0f;
//		beta1=*(uint16_t*)&info_g[1];
//		beta2=*(uint16_t*)&info_g[3]*100;
//		beta3=*(uint16_t*)&info_g[5]*1000;
		
		sendback=info_g[0];
		PrintChar(&sendback);
	}
}
