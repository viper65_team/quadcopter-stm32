#ifndef __CONTROL_H
#define __CONTROL_H

#include "main.h"
#include "quaternion.h"

void Control(void);
void UpdateXYPID(void);
void UpdateZPID(void);

extern __IO float kp,kd;
extern __IO float vx_acc,vy_acc;
extern __IO float x_z1,x_z2,x_z3,y_z1,y_z2,y_z3;
extern __IO float target_z;//used by pid
extern __IO quaternion attitude_c;
#endif
