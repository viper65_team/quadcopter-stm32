#include "attitude.h"
#include "arm_math.h"
__IO float yaw;
__IO char flag_bias=0;
__IO quaternion attitude_c;

#define abs(x) ( (x) >=0 ? (x) : ((-x)) )

void Calculate()//SR=1k
{  
	extern uint8_t result[14];
	static float acc_x,acc_y,acc_z,gryo_x,gryo_y,gryo_z,watchx,watchy,watchz;
	quaternion z;
	static float gryox_bias=1.74f,gryoy_bias=0.8f,gryoz_bias=-0.400f,bias_elimination;
	static int count;
	
	acc_x=((int16_t)(result[0]<<8)+result[1])/4095.875f;
	acc_y=((int16_t)(result[2]<<8)+result[3])/4095.875f;
	acc_z=((int16_t)(result[4]<<8)+result[5])/4095.875f;
	
	gryo_x=((int16_t)(result[8]<<8)+result[9])/16.3835f;//16.3835
	gryo_y=((int16_t)(result[10]<<8)+result[11])/16.3835f;//该轴的旋转不代表该轴偏角
	gryo_z=((int16_t)(result[12]<<8)+result[13])/16.3835f;//32767/2000

	//temp=((int16_t)(result[6]<<8)+result[7])/340+36.53;

//gryo bias	
	if(flag_bias)
	{
		if(flag_bias>1)
		{
			count=0;
			gryox_bias=0;
			gryoy_bias=0;
			gryoz_bias=0;
			flag_bias=1;
		}
		count++;
		gryox_bias+=gryo_x;
		gryoy_bias+=gryo_y;
		gryoz_bias+=gryo_z;
		if(count>=500)
		{
			gryox_bias/=count;
			gryoy_bias/=count;
			gryoz_bias/=count;
			flag_bias=0;
		}
		return;
	}		
	
	gryo_x-=gryox_bias;
	gryo_z-=gryoy_bias;
	gryo_z-=gryoz_bias+bias_elimination;
	
	watchx=gryo_x;
	watchy=gryo_y;
	watchz=gryo_z;

	gryo_x*=0.017453f;
	gryo_y*=0.017453f;
	gryo_z*=0.017453f;
	
//end gryo bias
	
//MadgwickAHRSupdateIMU(gryo_x,gryo_y,gryo_z,acc_x,acc_y,acc_z); 
	MahonyAHRSupdateIMU(gryo_x,gryo_y,gryo_z,acc_x,acc_y,acc_z);
	
	attitude_c.w=q0;
	attitude_c.x=q1;
	attitude_c.y=q2;
	attitude_c.z=q3;	
	
	//extract the yaw angle from c
	yaw=atan2(2*(attitude_c.w*attitude_c.z+attitude_c.x*attitude_c.y),1-2*(attitude_c.y*attitude_c.y+attitude_c.z*attitude_c.z));
	bias_elimination=yaw;
	z.w=arm_cos_f32(yaw/2);
	z.x=0;
	z.y=0;
	z.z=-arm_sin_f32(yaw/2);
	attitude_c=quatmulti(z,attitude_c);
	
	value[0]=q0*1000;
	value[1]=q1*1000;
	value[2]=q2*1000;
	value[3]=q3*1000;
	
//	value[7]=temp*100;
	
}
