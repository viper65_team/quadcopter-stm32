#include "manual_ctrl.h"

//float TargetValuesC[5]={1.0f,0.9993f,0.9972f,0.9937f,0.9888f};
//float TargetValuesS[5]={0.0f,0.0375f,0.0749f,0.1123f,0.1494f};
float TargetValuesC[5]={1.0f,0.9997f,0.9988f,0.9972f,0.9850f};
float TargetValuesS[5]={0.0f,0.0250f,0.0500f,0.0749f,0.0998f};
const float sqrt2INV=0.7071f;

__IO char flag_tChanged=0;
__IO float bias_yaw=0;
__IO quaternion t={1,0,0,0};//target attitude
#define abs(x) ( (x) >=0 ? (x) : ((-x)) )

void UpdateTarget()
{
		signed char dif,LV,LH,Z,state_x=0,state_y=0;
		static char state_xt=0,state_yt=0;
	
		if(info_g[1]+info_g[2]+info_g[3]==info_g[4])
		{
			LH=info_g[1];
			LV=info_g[2];
			Z=info_g[3];
			
			dif=abs(LV)-abs(LH);
			
			if(dif>1)//LV mode
			{
				if(LV<0)
				{
					t.w=TargetValuesC[-LV];
					t.x=TargetValuesS[-LV];
					t.y=0;
				}
				else
				{
					t.w=TargetValuesC[LV];
					t.x=-TargetValuesS[LV];
					t.y=0;
				}
				state_x=LV;
				state_y=0;
			}
			else 
				if(dif>-2)//LH+LV mode
				{
					if(LV>0&&LH>0)
					{
						LV=(LV+LH)/2;
						t.w=TargetValuesC[LV];
						t.x=-sqrt2INV*TargetValuesS[LV];
						t.y=sqrt2INV*TargetValuesS[LV];
					}
					if(LV>0&&LH<0)
					{
						LV=(LV-LH)/2;
						t.w=TargetValuesC[LV];
						t.x=-sqrt2INV*TargetValuesS[LV];
						t.y=-sqrt2INV*TargetValuesS[LV];
					}
					if(LV<0&&LH<0)
					{
						LV=-(LV+LH)/2;
						t.w=TargetValuesC[LV];
						t.x=sqrt2INV*TargetValuesS[LV];
						t.y=-sqrt2INV*TargetValuesS[LV];
					}
					if(LH>0&&LV<0)
					{
						LV=(-LV+LH)/2;
						t.w=TargetValuesC[LV];
						t.x=sqrt2INV*TargetValuesS[LV];
						t.y=sqrt2INV*TargetValuesS[LV];
					}
					if(LH==0&&LV==0)
					{
						t.w=1;
						t.x=0;
						t.y=0;
					}
					state_x=LV;
					state_y=LV;
				}
				else//LH mode
				{
					if(LH<0)
					{
						t.w=TargetValuesC[-LH];
						t.x=0;
						t.y=-TargetValuesS[-LH];
					}
					else
					{
						t.w=TargetValuesC[LH];
						t.x=0;
						t.y=TargetValuesS[LH];
					}
					state_x=0;
					state_y=LH;
				}
				
				if((state_xt!=state_x)||(state_yt!=state_y))
				{
					flag_tChanged=1;
				}
				state_xt=state_x;
				state_yt=state_y;
				
			//t.z=0.0f;
				
				bias_yaw+=Z/50.0f;
				if(bias_yaw>6.28f)
					bias_yaw-=6.28f;
				if(bias_yaw<-6.28f)
					bias_yaw+=6.28f;
			}
				
		if(info_g[5]+info_g[6]==info_g[7])		
		{
			Speed=info_g[5];
			if(info_g[6]==0x01)
			{
				flag_jeopardy=0;
			}
		}	
				
}
