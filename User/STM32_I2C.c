#include "STM32_I2C.h"

void i2cInit(void)
{
	I2C_InitTypeDef  I2C_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_I2CCLKConfig(RCC_I2C1CLK_SYSCLK);
	RCC_APB1PeriphClockCmd(	RCC_APB1Periph_I2C1, ENABLE); 
	RCC_AHBPeriphClockCmd(	RCC_AHBPeriph_GPIOB,	ENABLE);
	
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_4);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_4);

	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType=GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd=GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8|GPIO_Pin_7;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

	////////////////////////////////////////////////////IIC
	
	I2C_DeInit(I2C1);
  I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;  
  I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
  I2C_InitStructure.I2C_DigitalFilter = 0x00;
  I2C_InitStructure.I2C_Ack = I2C_Ack_Disable;
  I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
  I2C_InitStructure.I2C_Timing = 0xC0310409; //6M,

  I2C_Init(I2C1, &I2C_InitStructure);  
  I2C_Cmd(I2C1, ENABLE);
}

int8_t i2c_write(uint8_t addr_, uint8_t RegAddr, uint8_t len, uint8_t *value)
{    
  /* Test on BUSY Flag */
  uint32_t timeout = I2C_TIMEOUT;
  while(I2C_GetFlagStatus(I2C1, I2C_ISR_BUSY) != RESET)
  {
    if((timeout--) == 0) 
			return -1;
  }    
  I2C_TransferHandling(I2C1, (DeviceAddr & 0x7f)<<1, len+1, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);

  timeout = I2C_TIMEOUT;
  while(I2C_GetFlagStatus(I2C1, I2C_ISR_TXIS) == RESET) 
  {
    if((timeout--) == 0) 
		{
			return -1;          
		}
  }
	
	timeout = I2C_TIMEOUT;
	I2C_SendData(I2C1, RegAddr);
	
	while(len)
	{
		timeout = I2C_TIMEOUT;
		while(I2C_GetFlagStatus(I2C1, I2C_ISR_TXIS) == RESET) 
		{
			if((timeout--) == 0) 
				return -1;          
		}

		I2C_SendData(I2C1, *value);
		value++;
		len--;
	}
	
	while(I2C_GetFlagStatus(I2C1, I2C_ISR_STOPF) == RESET) 
  {
    if((timeout--) == 0) 
			return -1;          
  }

  I2C_ClearFlag(I2C1, I2C_ISR_STOPF);//I2C_ICR_STOPCF
	
	return 0;
}
/////////////////////////////////////////////////////////////////////////////////

int8_t i2cWrite(uint8_t addr, uint8_t reg, uint8_t data)
{
  /* Test on BUSY Flag */
  uint32_t timeout = I2C_TIMEOUT;
  while(I2C_GetFlagStatus(I2C1, I2C_ISR_BUSY) != RESET)
  {
    if((timeout--) == 0) 
			return -1;
  }    
  I2C_TransferHandling(I2C1, (DeviceAddr & 0x7f)<<1, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);

  timeout = I2C_TIMEOUT;
  while(I2C_GetFlagStatus(I2C1, I2C_ISR_TXIS) == RESET) 
  {
    if((timeout--) == 0) 
		{
			return -1;          
		}
  }
	
	timeout = I2C_TIMEOUT;
	I2C_SendData(I2C1, reg);
	
	while(I2C_GetFlagStatus(I2C1, I2C_ISR_TXIS) == RESET) 
  {
    if((timeout--) == 0) 
			return -1;          
  }
	
	timeout = I2C_TIMEOUT;
	I2C_SendData(I2C1, data);
	
	while(I2C_GetFlagStatus(I2C1, I2C_ISR_STOPF) == RESET) 
  {
    if((timeout--) == 0) 
			return -1;          
  }

  I2C_ClearFlag(I2C1, I2C_ICR_STOPCF);
	
	return 0;
}

int8_t i2c_read(uint8_t addr, uint8_t reg, uint8_t len, uint8_t *buf)
{
	/* Test on BUSY Flag */
  uint32_t timeout = I2C_TIMEOUT;
  while(I2C_GetFlagStatus(I2C1, I2C_ISR_BUSY) != RESET)
  {
    if((timeout--) == 0) 
			return -1;
  }    
	
  I2C_TransferHandling(I2C1, (DeviceAddr & 0x7f)<<1, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);

  timeout = I2C_TIMEOUT;
  while(I2C_GetFlagStatus(I2C1, I2C_ISR_TXIS) == RESET)
  {
    if((timeout--) == 0) 
		{
			return -1;          
		}
  }

	timeout = I2C_TIMEOUT;
	I2C_SendData(I2C1, reg);
	
	while(I2C_GetFlagStatus(I2C1, I2C_ISR_TC) == RESET)
  {
    if((timeout--) == 0)
			return -1;
  }

//receive	
	I2C_TransferHandling(I2C1, (DeviceAddr & 0x7f)<<1, len, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
	
	while(len)
	{
		timeout = I2C_TIMEOUT;
		while(I2C_GetFlagStatus(I2C1, I2C_ISR_RXNE) == RESET)
		{
			if((timeout--) == 0)
				return -1;
		}
		len--;
		*buf=I2C_ReceiveData(I2C1);
		buf++;
}
	timeout = I2C_TIMEOUT;
		while(I2C_GetFlagStatus(I2C1, I2C_ISR_STOPF) == RESET)
  {
    if((timeout--) == 0)
			return -1;
  }
	
	I2C_ClearFlag(I2C1,I2C_ISR_STOPF);
	return 0;
}
