#ifndef __TIM_H
#define __TIM_H

#include "stm32f30x.h"
#include <stm32f30x_tim.h>
#include "main.h"

void tim3_init(void);
void tim2_init(void);
void tim4_init(void);

 __inline void TIM2_PWMENABLE(void)
 {
 	TIM2->CCER|=0x1111;
 }
 
 __inline void TIM2_PWMDISABLE(void)
 {
 	TIM2->CCER&=0xEEEE;
 }
 
 __inline void TIM2_Ch1(float value)
 {
 	TIM2->CCR1=(int)(value*64.0f);
 }
 
 __inline void TIM2_Ch2(float value)
 {
 	TIM2->CCR2=(int)(value*64.0f);
 }
 
 __inline void TIM2_Ch3(float value)
 {
 	TIM2->CCR3=(int)(value*64.0f);
 }
 
 __inline void TIM2_Ch4(float value)
 {
 	TIM2->CCR4=(int)(value*64.0f);
 }
//keil 中 内联函数不具有外联性，不允许c文件中的内联函数被外部文件引用
//但是在头文件中这样是可以被使用的。

extern __IO uint8_t flag_calculate,commucating_flag,noComm_flag;
//变量被外部文件使用时
//需要将定义放在。c文件里
//在头文件中做一个extern的声明
//这样就可以避免重复定义
//因为ifdef的条件只对本C文件有效
//多个c文件仍然会重复编译
#endif
